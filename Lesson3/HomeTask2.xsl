<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                exclude-result-prefixes="xs"
                version="1.0">
    
    <xsl:output method="xml" indent="yes"/>
    <xsl:strip-space elements="*" />
    
    <xsl:key name="nodesGroup" match="*|@*" use="name()"/>
      
    <xsl:template match="/">
        <xsl:variable name="union" 
                      select="//*[count(.|//@*) != count(//@*)] | //@*[count(.|//*) != count(//*)]"/>
        
        <xsl:apply-templates select="$union[generate-id(.) = generate-id(key('nodesGroup', name()))]"/>      
    </xsl:template>
    
    <xsl:template match="*|@*">
        <xsl:variable name="apos" select='"&apos;"'/>  
        
        <xsl:value-of select="concat('&#xa;', 'Node ', $apos, name(), $apos, ' found ', count(key('nodesGroup', name())), ' times.')"/>      
     </xsl:template>

</xsl:stylesheet>