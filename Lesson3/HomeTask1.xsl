<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                exclude-result-prefixes="xs"
                version="1.0">
    
    <xsl:output method="xml" indent="yes"/>
    <xsl:strip-space elements="*" />
    
    <xsl:variable name="lower" select="'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'"/>
    <xsl:variable name="upper" select="'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'"/>
    
    <xsl:key name="namesGroup" match="item" use="translate(substring(@Name, 1, 1), 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя', 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ')"/>
    
    <xsl:template match="list">
        <list>
            <xsl:apply-templates select="item[generate-id(.) = generate-id(key('namesGroup', translate(substring(@Name, 1, 1), $lower, $upper)))]">
                <xsl:sort select="@Name"/>
            </xsl:apply-templates>
        </list>
    </xsl:template>
    
    <xsl:template match="item">
        <capital value="{substring(@Name, 1, 1)}">
            <xsl:for-each select="key('namesGroup', translate(substring(@Name, 1, 1), $lower, $upper))">
                <name>
                    <xsl:call-template name="WithCapitalLetter">
                        <xsl:with-param name="str" select="@Name"/>
                    </xsl:call-template>
                </name>
            </xsl:for-each>
        </capital>
    </xsl:template>
    
    <xsl:template name="WithCapitalLetter">
        <xsl:param name="str"/>
        
        <xsl:value-of select="concat(translate(substring($str, 1, 1), $lower, $upper), translate(substring($str, 2), $upper, $lower))"/>
    </xsl:template>
    
</xsl:stylesheet>