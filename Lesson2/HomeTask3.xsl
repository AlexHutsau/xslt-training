<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:def="xml_version_1.0"
                xmlns:end="https://www.meme-arsenal.com/create/template/43024"
                exclude-result-prefixes="xs def"
                version="1.0">
                
    <xsl:output method="xml" indent="yes"/>
    <xsl:strip-space elements="*" />
    
    <xsl:template match="*">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="def:Guests">
        <Peoples xmlns="xml_version_1.0"
                 xmlns:end="https://www.meme-arsenal.com/create/template/43024">
            <xsl:apply-templates select="@* | node()"/>           
        </Peoples>
    </xsl:template>
    
    <xsl:template match="def:Guest">
        <xsl:element name="People" namespace="xml_version_1.0">
            <xsl:apply-templates select="@* | node()"/>           
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="end:Guest">
        <xsl:element name="end:People" namespace="https://www.meme-arsenal.com/create/template/43024">
            <xsl:apply-templates select="@* | node()"/>           
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="@*">
        <xsl:variable name="lower" select="'abcdefghijkmnopqrstuvyxwz'"/>
        <xsl:variable name="upper" select="'ABCDEFGHIJKMNOPQRSTUVYXWZ'"/>
        
        <xsl:attribute name="{translate(name(), $lower, $upper)}">
            <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>
    
    <xsl:template match="text()">
        <Text xmlns="xml_version_1.0">
            <xsl:value-of select="."/>
        </Text>
    </xsl:template>
</xsl:stylesheet>