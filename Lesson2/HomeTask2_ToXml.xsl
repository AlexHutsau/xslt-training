<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns="xml_version_1.0"
                xmlns:end="https://www.meme-arsenal.com/create/template/43024"
                exclude-result-prefixes="xs" version="1.0">
                
    <xsl:output method="xml" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="/">
        <Guests xmlns="xml_version_1.0"
                xmlns:end="https://www.meme-arsenal.com/create/template/43024">
            <xsl:call-template name="split1">
                <xsl:with-param name="str" select="."/>
            </xsl:call-template>
        </Guests>

    </xsl:template>

    <xsl:template name="split1">
        <xsl:param name="str"/>
        <xsl:variable name="delimeter" select="'|'"/>

        <xsl:if test="contains($str, $delimeter)">
            <xsl:element name="{substring-before(substring-before($str, $delimeter), '#')}Guest">
                <xsl:call-template name="split2">
                    <xsl:with-param name="str"
                        select="substring-after(substring-before($str, $delimeter), '#')"/>
                </xsl:call-template>
            </xsl:element>

            <xsl:if test="substring-after($str, $delimeter) != ''">
                <xsl:call-template name="split1">
                    <xsl:with-param name="str" select="substring-after($str, $delimeter)"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="split2">
        <xsl:param name="str"/>
        <xsl:variable name="delimeter" select="'/'"/>
        
        <xsl:if test="$str != ''">
            <xsl:variable name="age" select="substring-before($str, $delimeter)"/>
            <xsl:variable name="nationality" select="substring-before(substring-after($str, concat($age, $delimeter)), $delimeter)"/>
            <xsl:variable name="gender" select="substring-before(substring-after($str, concat($nationality, $delimeter)), $delimeter)"/>
            <xsl:variable name="name" select="substring-before(substring-after($str, concat($gender, $delimeter)), $delimeter)"/>
            <xsl:variable name="type" select="substring-before(substring-after($str, concat($name, $delimeter)), $delimeter)"/>
            <xsl:variable name="address" select="substring-before(substring-after($str, concat($type, $delimeter)), $delimeter)"/>
            <xsl:variable name="email" select="substring-after($str, concat($address, $delimeter))"/>
        
            <xsl:if test="$age != 'null'">
                <xsl:attribute name="Age">
                    <xsl:value-of select="$age"/>
                </xsl:attribute>
            </xsl:if>
            
            <xsl:if test="$nationality != 'null'">
                <xsl:attribute name="Nationalty">
                    <xsl:value-of select="$nationality"/>
                </xsl:attribute>
            </xsl:if>
            
            <xsl:if test="$gender != 'null'">
                <xsl:attribute name="Gender">
                    <xsl:value-of select="$gender"/>
                </xsl:attribute>
            </xsl:if>
            
            <xsl:if test="$name != 'null'">
                <xsl:attribute name="Name">
                    <xsl:value-of select="$name"/>
                </xsl:attribute>
            </xsl:if>
            
            <Type>
                <xsl:if test="$type != 'null'">
                    <xsl:value-of select="$type"/>
                </xsl:if>
            </Type>
            
            <Profile>
                <xsl:if test="$address != 'null'">
                    <Address>
                        <xsl:value-of select="$address"/>
                    </Address>
                </xsl:if>
                <xsl:if test="$email != 'null'">
                    <Email>
                        <xsl:value-of select="$email"/>
                    </Email>
                </xsl:if>
            </Profile>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>