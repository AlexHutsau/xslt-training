<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                exclude-result-prefixes="xs"
                version="1.0">
    
    <xsl:output method="xml" indent="yes"/>
    <xsl:strip-space elements="*" />
    
    <xsl:template match="/">
        <string>
            <xsl:apply-templates select="*"/>
        </string>
    </xsl:template>
    
    <xsl:template match="*[local-name() = 'Guest']">
        
        <xsl:variable name="np" select="namespace-uri(.)"/>
        
        <xsl:choose>
            <xsl:when test="$np = 'xml_version_1.0'">
                <xsl:text>#</xsl:text>
            </xsl:when>
            <xsl:when test="$np = 'https://www.meme-arsenal.com/create/template/43024'">
                <xsl:text>end:#</xsl:text>
            </xsl:when>
        </xsl:choose>
        
        <xsl:choose>
            <xsl:when test="@Age != ''">
                <xsl:value-of select="@Age" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="'null'" />
            </xsl:otherwise>
        </xsl:choose>
        
        <xsl:value-of select="'/'" />
        
        <xsl:choose>
            <xsl:when test="@Nationalty != ''">
                <xsl:value-of select="@Nationalty" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="'null'" />
            </xsl:otherwise>
        </xsl:choose>
        
        <xsl:value-of select="'/'" />
        
        <xsl:choose>
            <xsl:when test="@Gender != ''">
                <xsl:value-of select="@Gender" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="'null'" />
            </xsl:otherwise>
        </xsl:choose>
        
        <xsl:value-of select="'/'" />
        
        <xsl:choose>
            <xsl:when test="@Name != ''">
                <xsl:value-of select="@Name" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="'null'" />
            </xsl:otherwise>
        </xsl:choose>
        
        <xsl:value-of select="'/'" />
        
        <xsl:choose>
            <xsl:when test="./*[local-name() = 'Type'] != ''">
                <xsl:value-of select="./*[local-name() = 'Type']" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="'null'" />
            </xsl:otherwise>
        </xsl:choose>
        
        <xsl:value-of select="'/'" />
        
        <xsl:choose>
            <xsl:when test="./*[local-name() = 'Profile']/*[local-name() = 'Address'] != ''">
                <xsl:value-of select="./*[local-name() = 'Profile']/*[local-name() = 'Address']" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="'null'" />
            </xsl:otherwise>
        </xsl:choose>
        
        <xsl:value-of select="'/'" />
        
        <xsl:choose>
            <xsl:when test="./*[local-name() = 'Profile']/*[local-name() = 'Email'] != ''">
                <xsl:value-of select="./*[local-name() = 'Profile']/*[local-name() = 'Email']" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="'null'" />
            </xsl:otherwise>
        </xsl:choose>
        
        <xsl:value-of select="'|'" />       
    </xsl:template>
    
</xsl:stylesheet>