<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="1.0">
<xsl:output method="xml" indent="yes"/>
<xsl:strip-space elements="*" />

    <xsl:template match="/">
        <result>
            <xsl:text>&#xa;</xsl:text>
                    
            <xsl:for-each select="//*[local-name() = 'Guest']">
                <xsl:if test="@Name = 'Jimmy'">
                    <xsl:for-each select="preceding-sibling::*">                      
                        <xsl:value-of select="translate(@Name, 'ABCDEFGHIJKMNOPQRSTUVYXWZ', 'abcdefghijkmnopqrstuvyxwz')"/>
                        <xsl:text>&#xa;</xsl:text>
                    </xsl:for-each>
                    
                    <xsl:text>&#xa;-------------&#xa;&#xa;</xsl:text>
                    
                    <xsl:for-each select="following-sibling::*">
                        <xsl:if test="@Nationalty != 'BY'">
                            <xsl:value-of select="./*[local-name() = 'Profile']/*[local-name() = 'Address']"/>
                            <xsl:text>&#xa;</xsl:text>
                        </xsl:if>
                    </xsl:for-each>           
                </xsl:if>
            </xsl:for-each>
            
            <xsl:text>&#xa;-------------&#xa;&#xa;</xsl:text>
            
            <xsl:for-each select="//*[local-name() = 'Guest']">
                <xsl:if test="not(contains(*[local-name() = 'Profile']/*[local-name() = 'Address'], 'Pushkinskaya'))">
                    <Address>
                        <xsl:attribute name="Name">
                            <xsl:value-of select="@Name"/>
                        </xsl:attribute>
                        <xsl:attribute name="Nationalty">
                            <xsl:value-of select="@Nationalty"/>
                        </xsl:attribute>
                        <xsl:value-of select="*[local-name() = 'Profile']/*[local-name() = 'Address']"/>
                    </Address>                   
                </xsl:if>
            </xsl:for-each>           
        </result>
    </xsl:template>
     
</xsl:stylesheet>