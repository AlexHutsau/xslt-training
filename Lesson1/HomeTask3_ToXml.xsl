<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="1.0">
<xsl:output method="xml" indent="yes"/>
<xsl:strip-space elements="*" />
    
    <!--
        Особенности выполнения задания...

        1. Не успел придумать альернативу функции tokenize(string, string) для XSLT 1.0.

        2. В сравнении с оригианальным Guests.xml не сохранил ссылки на пространства имен. А надо ли было?
    -->
    <xsl:template match="/">
        <Guests>
            <xsl:for-each select="tokenize(., '\|')">
                <xsl:if test=". != ''">
                    <Guest>
                        <xsl:variable name="details" select="tokenize(., '/')"/>
                        
                        <xsl:if test="$details[1] != 'null'">
                            <xsl:attribute name="Age"><xsl:value-of select="$details[1]"/></xsl:attribute>
                        </xsl:if>
                        
                        <xsl:if test="$details[2] != 'null'">
                            <xsl:attribute name="Nationalty"><xsl:value-of select="$details[2]"/></xsl:attribute>
                        </xsl:if>
                        
                        <xsl:if test="$details[3] != 'null'">
                            <xsl:attribute name="Gender"><xsl:value-of select="$details[3]"/></xsl:attribute>
                        </xsl:if>
                        
                        <xsl:if test="$details[4] != 'null'">
                            <xsl:attribute name="Name"><xsl:value-of select="$details[4]"/></xsl:attribute>
                        </xsl:if>
                        
                        <Type>
                            <xsl:if test="$details[5] != 'null'">
                                <xsl:value-of select="$details[5]"/>
                            </xsl:if>
                        </Type>
                        
                        <Profile>
                            <xsl:if test="$details[6] != 'null'">
                                <Address><xsl:value-of select="$details[6]"/></Address>
                            </xsl:if>
                            <xsl:if test="$details[7] != 'null'">
                                <Email><xsl:value-of select="$details[7]"/></Email>
                            </xsl:if>
                        </Profile>
                    </Guest>
                </xsl:if>
            </xsl:for-each>
        </Guests>
        
    </xsl:template>
    
</xsl:stylesheet>